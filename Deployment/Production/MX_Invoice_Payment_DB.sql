USE [IMGlobalWebMX_transactions]
GO

 /* [dbo].[DocumentFiles] */
CREATE TABLE [dbo].[DocumentFiles](
	[IdDocument] [bigint] NOT NULL,
	[docNumber] [bigint] NULL,
	[IdDocType] [int] NOT NULL,
	[dctDescription] [varchar](50) NULL,
	[docDate] [datetime] NOT NULL,
	[docReceiver] [varchar](14) NOT NULL,
	[docAmount] [decimal](18, 6) NOT NULL,
	[docNetAmount] [decimal](18, 6) NOT NULL,
	[docTaxAmount] [decimal](18, 6) NOT NULL,
	[docFileName] [varchar](200) NULL,
	[FilePDF] [varbinary](max) NULL,
	[FileXML] [varbinary](max) NULL,
	[docSerie] [varchar](25) NULL,
	[CompanyCode] [varchar](3) NULL,
	[IMPulseInvoiceNumber] [varchar](9) NULL,
	[InvoiceDate] [date] NULL,
	[DueDate] [date] NULL,
	[BillToBranchCustomerNumber] [varchar](8) NULL,
	[CustomerOrderNumber] [varchar](25) NULL,
	[TotalForeignSales] [money] NULL,
	[TotalSales] [money] NULL,
	[CurrencyCode] [varchar](3) NULL,
 CONSTRAINT [PK_IdDocument] PRIMARY KEY CLUSTERED 
(
	[IdDocument] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = OFF, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

GRANT SELECT,INSERT,UPDATE,DELETE,ALTER ON [dbo].[DocumentFiles] TO PUBLIC
GO

 /* [dbo].[ResellerAccountSummary] */

CREATE TABLE [dbo].[ResellerAccountSummary](
	[CompanyCode] [varchar](2) NULL,
	[BranchCustomerNumber] [varchar](10) NULL,
	[CustomerName] [varchar](100) NULL,
	[CreditManagerName] [varchar](100) NULL,
	[SalesAssociateName] [varchar](100) NULL,
	[CreditLimit] [money] NULL,
	[Past Due 1 - 15] [money] NULL,
	[Past Due 16 - 30] [money] NULL,
	[Past Due 31 - 60] [money] NULL,
	[Past Due 61 - 90] [money] NULL,
	[Past Due 91] [money] NULL,
	[TotalPastDue] [money] NULL,
	[CurrentDue] [money] NULL,
	[TotalBalanceAmount] [money] NULL,
	[AsDate] [datetime] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[ResellerAccountSummary] ADD [CreditManagerEmail] [varchar](200) NULL

GO

GRANT SELECT,INSERT,UPDATE,DELETE,ALTER ON [dbo].[ResellerAccountSummary] TO PUBLIC
GO

 /* [dbo].[ResellerDueInvoices] */

CREATE TABLE [dbo].[ResellerDueInvoices](
	[CompanyCode] [varchar](2) NULL,
	[InvoiceNumber] [varchar](20) NULL,
	[InvoiceDate] [datetime] NULL,
	[DueDate] [datetime] NULL,
	[BillToBranchCustomerNumber] [varchar](20) NULL,
	[CustomerOrderNumber] [varchar](100) NULL,
	[TotalForeignSalesAmount] [money] NULL,
	[TotalSalesAmount] [money] NULL,
	[AsDate] [datetime] NULL
) ON [PRIMARY]

GO

GRANT SELECT,INSERT,UPDATE,DELETE,ALTER ON [dbo].[ResellerDueInvoices] TO PUBLIC
GO

 /* [dbo].[sp_GetEbillDetails] */
 
CREATE PROCEDURE [dbo].[sp_GetEbillDetails]
(
  /* Input Parameters */
  @DocReceiver VARCHAR(14),
  @DocNumber BIGINT = NULL,
  @InvBeginDate DATETIME = NULL, 
  @InvEndDate DATETIME = NULL,
  @DueBeginDate DATETIME = NULL, 
  @DueEndDate DATETIME = NULL,
  @DocTypeID int = NULL,
  @CurrencyCode VARCHAR(3)=NULL,
   /* Pagination Parameters */
  @PageNo INT = 1,
  @PageSize INT = 10,
   /* Sorting Parameters */
  @SortColumn NVARCHAR(20) = 'docNumber',
  @SortOrder NVARCHAR(4)= 'ASC',
  @ExchangeRate DECIMAL(18,2)
)
AS
BEGIN
    /*Declaring Local Variables corresponding to parameters for modification */
    DECLARE 
    @lDocReceiver VARCHAR(14),
	@lDocNumber BIGINT,
	@lInvBeginDate DATETIME, 
	@lInvEndDate DATETIME,
	@lDueBeginDate DATETIME, 
	@lDueEndDate DATETIME, 
	@lDocTypeID int,
	@lCurrencyCode VARCHAR(3), 
    @lPageNbr INT,
    @lPageSize INT,
    @lSortCol NVARCHAR(20),
    @lFirstRec INT,
    @lLastRec INT,
    @lTotalRows INT

    /*Setting Local Variables*/

	SET	@lDocReceiver = LTRIM(RTRIM(@DocReceiver))
	SET @lDocNumber = @DocNumber
	SET @lInvBeginDate = @InvBeginDate
	SET @lInvEndDate = @InvEndDate
	SET @lDueBeginDate = @DueBeginDate
	SET @lDueEndDate = @DueEndDate
	SET @lDocTypeID = @DocTypeID
	SET @lCurrencyCode =LTRIM(RTRIM(@CurrencyCode)) 

    SET @lPageNbr = @PageNo
    SET @lPageSize = @PageSize
    SET @lSortCol = LTRIM(RTRIM(@SortColumn))

    SET @lFirstRec = ( @lPageNbr - 1 ) * @lPageSize
    SET @lLastRec = ( @lPageNbr * @lPageSize + 1 )
    SET @lTotalRows = @lFirstRec - @lLastRec + 1

    ; WITH Doc_Results
    AS (
    SELECT ROW_NUMBER() OVER (ORDER BY
        CASE WHEN (@lSortCol = 'docNumber' AND @SortOrder='ASC')
                    THEN docNumber
        END ASC,
        CASE WHEN (@lSortCol = 'docNumber' AND @SortOrder='DESC')
                   THEN docNumber
        END DESC,
        CASE WHEN (@lSortCol = 'docDate' AND @SortOrder='ASC')
                    THEN docDate
        END ASC,
        CASE WHEN (@lSortCol = 'docDate' AND @SortOrder='DESC')
                   THEN docDate
        END DESC,
        CASE WHEN (@lSortCol = 'IdDocType' AND @SortOrder='ASC')
                    THEN IdDocType
        END ASC,
        CASE WHEN (@lSortCol = 'IdDocType' AND @SortOrder='DESC')
                   THEN IdDocType
        END DESC,
        CASE WHEN (@lSortCol = 'TotalSales' AND @SortOrder='ASC')
                    THEN TotalSales
        END ASC,
        CASE WHEN (@lSortCol = 'TotalSales' AND @SortOrder='DESC')
                   THEN TotalSales
        END DESC,
        CASE WHEN (@lSortCol = 'TotalForeignSales' AND @SortOrder='ASC')
                    THEN TotalForeignSales
        END ASC,
        CASE WHEN (@lSortCol = 'TotalForeignSales' AND @SortOrder='DESC')
                   THEN TotalForeignSales
        END DESC,
         CASE WHEN (@lSortCol = 'DueDate' AND @SortOrder='ASC')
                    THEN DueDate
        END ASC,
        CASE WHEN (@lSortCol = 'DueDate' AND @SortOrder='DESC')
                   THEN DueDate
        END DESC        
   ) AS ROWNUM,
   Count(*) over () AS TotalCount,IdDocument,
   docNumber,CASE(IdDocType) WHEN 33 THEN 'Invoice'
              WHEN 61 THEN 'CreditNote'
              WHEN 71 THEN 'InterestBill'
              END As IdDocType
              ,docDate,docAmount,docTaxAmount,docSerie,docreceiver,CurrencyCode,DueDate,
              [TotalMXP] = (CASE ISNULL(TotalForeignSales,'0.00') 
							WHEN '0.00' THEN TotalSales  ELSE (TotalForeignSales * @ExchangeRate) END),
			  [TotalUSD] = ISNULL(TotalForeignSales,'0.00')              
 FROM DocumentFiles
 WHERE
 docReceiver=@lDocReceiver
  AND (@lDocReceiver IS NULL OR docReceiver = @lDocReceiver)
  AND (@lDocNumber IS NULL OR docNumber = @lDocNumber)
  AND (@lDocTypeID IS NULL OR IdDocType = @lDocTypeID)
  AND (docDate >= @lInvBeginDate OR @lInvBeginDate IS NULL)
  AND (docDate <= @lInvEndDate + 1 OR @lInvEndDate IS NULL)
  AND (DueDate >= @lDueBeginDate OR @lDueBeginDate IS NULL)
  AND (DueDate <= @lDueEndDate + 1  OR @lDueEndDate IS NULL)
  AND (@lCurrencyCode IS NULL OR CurrencyCode = @lCurrencyCode)
)
SELECT IdDocument,docNumber,IdDocType,docDate,docAmount,docTaxAmount,docSerie,docReceiver,CurrencyCode,DueDate,TotalCount,TotalMXP,TotalUSD,
    ROWNUM
FROM Doc_Results AS DOC
WHERE ROWNUM > @lFirstRec
	  AND ROWNUM < @lLastRec
ORDER BY ROWNUM ASC
END

GO

GRANT ALTER,EXECUTE ON [dbo].[sp_GetEbillDetails] TO PUBLIC 
GO

/* [dbo].[sp_GetPDFData] */

CREATE PROCEDURE [dbo].[sp_GetPDFData]
(
	@docNumber bigint,
	@docReceiver varchar(14)
)
AS
BEGIN
    DECLARE @lDocReceiver VARCHAR(14)
	SET	@lDocReceiver = LTRIM(RTRIM(@docReceiver))
	
	SELECT [FilePDF]
	FROM [dbo].[DocumentFiles]
	WHERE docNumber = @docNumber
	AND docReceiver= @lDocReceiver
END
GO

GRANT ALTER,EXECUTE ON [dbo].[sp_GetPDFData] TO PUBLIC 
GO

/* [dbo].[sp_GetXMLData] */

CREATE PROCEDURE [dbo].[sp_GetXMLData]
(
	@docNumber bigint,
	@docReceiver varchar(14)
)
AS
BEGIN
	DECLARE     @lDocReceiver VARCHAR(14)
	SET	@lDocReceiver = LTRIM(RTRIM(@docReceiver))
	
	SELECT [FileXML]
	FROM [dbo].[DocumentFiles]
	WHERE docNumber = @docNumber
	AND docReceiver= @lDocReceiver
END

GO

GRANT ALTER,EXECUTE ON [dbo].[sp_GetXMLData] TO PUBLIC 
GO

/* [dbo].[sp_GetAROpenCreditInvoices] */

CREATE PROCEDURE [dbo].[sp_GetAROpenCreditInvoices]
(
	@BranchCustomerNumber varchar(20)
)
AS
BEGIN
    DECLARE     @lBranchCustomerNumber VARCHAR(14)
	SET	@lBranchCustomerNumber = LTRIM(RTRIM(@BranchCustomerNumber))
	
	SELECT  REPLACE(LTRIM(REPLACE(SUBSTRING(invoicenumber,3,LEN(invoicenumber)), '0', ' ')), ' ', '0') as InvoiceNumber  
	FROM [dbo].[ResellerDueInvoices]
	WHERE BillToBranchCustomerNumber = @lBranchCustomerNumber 
END
GO

GRANT ALTER,EXECUTE ON [dbo].[sp_GetAROpenCreditInvoices] TO PUBLIC 
GO

/* [dbo].[sp_GetAROpenCreditData] */

CREATE PROCEDURE [dbo].[sp_GetAROpenCreditData]
(
	@BranchCustomerNumber varchar(20),
	@ExchangeRate DECIMAL(18,2)
)
AS
BEGIN
    DECLARE @lBranchCustomerNumber VARCHAR(20)
	SET	@lBranchCustomerNumber = LTRIM(RTRIM(@BranchCustomerNumber))
	
	SELECT  REPLACE(LTRIM(REPLACE(SUBSTRING(invoicenumber,3,LEN(invoicenumber)), '0', ' ')), ' ', '0') as InvoiceNumber	 
      ,[InvoiceDate]
      ,[DueDate]
      ,[BillToBranchCustomerNumber]
      ,[CustomerOrderNumber]
      ,[TotalForeignSalesAmount]
      ,[TotalSalesAmount] = (CASE TotalForeignSalesAmount  
 WHEN '0.00' THEN TotalSalesAmount  ELSE (TotalForeignSalesAmount * @ExchangeRate) END) 	
	FROM [dbo].[ResellerDueInvoices]
	WHERE BillToBranchCustomerNumber = @lBranchCustomerNumber and TotalSalesAmount < 0 
    Order By DueDate 
END

GO

GRANT ALTER,EXECUTE ON [dbo].[sp_GetAROpenCreditData] TO PUBLIC 
GO

/* [dbo].[UserObject] */

USE [IMGlobalWebMX_Profiles]
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'u_ebill_notification_email' AND TABLE_NAME = 'UserObject' )
BEGIN
	ALTER TABLE dbo.UserObject ADD u_ebill_notification_email varchar(max)
END
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'TAX_EXEMPT_NBR' AND TABLE_NAME = 'UserObject' )
BEGIN
	ALTER TABLE dbo.UserObject ADD TAX_EXEMPT_NBR varchar(20)
END
GO

/* [dbo].[InsertUpdateEbillEmails] */

USE [IMGlobalWebMX_Profiles]
GO

/****** Object:  StoredProcedure [dbo].[InsertUpdateEbillEmails]    Script Date: 07/06/2016 01:32:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertUpdateEbillEmails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertUpdateEbillEmails]
GO

USE [IMGlobalWebMX_Profiles]
GO

/****** Object:  StoredProcedure [dbo].[InsertUpdateEbillEmails]    Script Date: 07/06/2016 01:32:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertUpdateEbillEmails]
	(
	@ResellerId	NVARCHAR(50),
	@EmailIds	NVARCHAR(200),
	@TaxExmptNo	NVARCHAR(20),
	@UserId		NVARCHAR(50)
	)
AS
BEGIN

	IF Not Exists (SELECT * FROM UserObject WHERE u_reseller_id = @ResellerId and u_user_id=@UserId)
   		INSERT INTO UserObject
   		(u_user_id, u_reseller_id, u_ebill_notification_email,TAX_EXEMPT_NBR)
		VALUES
		(@UserId, @ResellerId, @EmailIds, @TaxExmptNo)
    ELSE
		UPDATE UserObject
		SET u_ebill_notification_email = @EmailIds,
			TAX_EXEMPT_NBR=@TaxExmptNo
		WHERE u_reseller_id = @ResellerId and u_user_id=@UserId
    
END

GO


USE [IMGlobalWebMX_Profiles]
GO

GRANT ALTER,EXECUTE ON [dbo].[InsertUpdateEbillEmails] TO PUBLIC 
GO
